#ifndef MENU_H

#include "config.h"

#include <MLV/MLV_all.h>
#include <time.h>
#include "game.h"
#include "credit.h"
#include "score.h"

void menu(); /* creer le menu */
int afficheMenu(); /* affiche le menu */
void afficheFondMenu(); /* affiche le fond du menu */
void afficheOption(); /* affiche les options */
int choixOption(); /* recupere le choix du menu */


#endif // MENU_H
