#ifndef GAME_H

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <MLV/MLV_all.h>

#include "joueur.h"
#include "ennemi.h"
#include "case.h"
#include "config.h"

typedef struct{

    Joueur heros; /* le heros */
    Ennemi * tas; /* le tas d'ennemie basic */
    Ennemi bosse; /* le bosse pour chaque niveau */
    Case plateau[PMAX][PMAX]; /* le plateau */
    int level; /* niveau du joueur */
    int score; /* scores obtenu */
    int lvlInfo[MAXLEVEL]; /* voir si les niveaux sont ouverts ou ferme */
    int nbEnnemi; /* nb ennemi , level * difficulte */
    int nbEnnemiInit; /* nombre ennemi initiale */

}theGame;


void chooseLevel(); /* on choisit son niveau */
void setLevel(theGame *level); /* on regle le level */
void levelC(theGame *level); /* on rentre le level */
void drawLevel(theGame *level); /* dessine les niveaux */
void chooseMe(theGame *level); /* on choisit le level */
void theGameRunner(theGame *level); /* lance la partie */
void cleanFire(theGame *level); /* nettoie les tirs */
void cleanCollision(theGame *level); /* nettoie les collisions */
void showWorld(theGame *level); /* montre le jeu avec la mlv */
void showStat(theGame *level); /* montre les stats en bas */
void ennemiShow(theGame *level); /* fait apparaitre les ennemis et les fait bouger */
void ennemiS(theGame *level); /* on decide si on fait appraitre un ennemi ou pas */
int win(theGame *level); /* retourne si on a gagné ou pas */

#endif // GAME_H
