#ifndef ENNEMI_H

#include <time.h>
#include <stdlib.h>
#include "config.h"



typedef struct {

    int posx; /* position de l'ennemi */
    int posy; /* position de l'ennemi */
    int vie; /* pour les supers bosses etc */
    int bouclier ;/* pour les supers bosses etc */

}Ennemi;

int ennemi(Ennemi *level,int size, int x , int y); /* retourne l'indice du tableau correspondant a l'ennemi */
int ennemiMove( int h, int b , int g, int y); /* renvoie une action de l'ennemi , 0 tire , 1 haut , bas , gauche */
/* on lui envoie ce qu'il y a en haut , en bas , a gauche et sa position en y */

#endif // ENNEMI_H
