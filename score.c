#include "score.h"


void afficheScore(){

    MLV_draw_filled_rectangle(0,0,RESX,RESY,MLV_COLOR_BLACK);


    FILE *file;
    char tmp[PMAX] = { '\0',};
    double i = 0.00;

    file = fopen("score.txt","r");

    if( file != NULL){

        while(fgets(tmp,39,file) != NULL){

            MLV_draw_text(RESX*(0.50), RESY*(0.50+i),tmp,MLV_COLOR_BLUE);
            i += 0.05;

        }
        MLV_draw_text(RESX*(0.50), RESY*(0.50+i),"Clic pour quitter ...", MLV_COLOR_RED);
        int a ,b ;
        MLV_wait_mouse(&a,&b);
        MLV_actualise_window();
          MLV_wait_mouse(&a,&b);
    }
    else{
        fprintf(stderr,"Erreur ouverture score ... \n");
        return;
    }


}
