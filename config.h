#ifndef CONFIG_H


#define PMAX 40    /*taille plateau*/
#define RESX 1280 /* taille ecran */
#define RESY 720 /* taille ecran */
#define nomFenetre "ShootEmUp"
#define nomIcone "ShootEmUp"
#define MAXLEVEL 30 /* nombre de niveaux */
#define BOSSELVL 5 /* quand apparait le bosse, ici si X = 5 , tout les 5 niveaux réussis */
#define MAXCHAR 10 /* maximum char dans un tab */
#define DEBUG 1 /* debug si il le faut */
#define DIFFICULTE 5 /* regler la diffuclter ici */
#define VIEJOUEUR 10 /* la vie du joueur */
#define BOUCLIERJOUEUR 10 /* le boucleir du joueur */
#define POPULATION 5 /* population du joueur */
#define BILLION 1000000000.0
#define FPS 70.0

#endif // CONFIG_H
