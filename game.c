#include "game.h"



void chooseLevel(){

/* le jeu se passe ici */

    theGame level;

    levelC(&level);
    setLevel(&level);
/* on parametre le niveaux et c'est parti */

    if(DEBUG){
    printf("%d   level in game\n",level.level);
    }

    /* on lance si pas d'erreur */
    theGameRunner(&level);
}




void setLevel(theGame *level){

    (*level).score = 0;
    /* set score */
    (*level).heros.bouclier = BOUCLIERJOUEUR;
    (*level).heros.population = POPULATION;
    (*level).heros.vie = VIEJOUEUR;
    (*level).heros.posx = 0; /* juste au milieu */
    (*level).heros.posy = 20;

    /* set ennemi */
    (*level).nbEnnemi = DIFFICULTE * (*level).level;
    (*level).nbEnnemiInit = (*level).nbEnnemi;
    (*level).tas = (Ennemi*) malloc(sizeof(Ennemi) * (*level).nbEnnemi );

    if( (*level).tas == NULL){
        fprintf(stderr,"Erreur allocation ennemi , closing ... \n");
        exit(EXIT_FAILURE);
    }

    int i;

    for(i=0; i < (*level).nbEnnemi ; i++){
        (*level).tas[i].bouclier = 0;
        (*level).tas[i].posx = -1;
        (*level).tas[i].posy = -1;
        (*level).tas[i].vie = 1;
    }

    /* set bosse  tout les 5 niveaux */
    if( ((*level).level)%5 == 0 ){
        (*level).bosse.bouclier = DIFFICULTE * (*level).level;
        (*level).bosse.vie = DIFFICULTE * (*level).level;
        (*level).bosse.posx = -1;
        (*level).bosse.posy = -1;
    }
    else{
        (*level).bosse.vie = 0;
    }

    /* on regle le plateau */
    int j;

    for(i=0; i < PMAX ; i++){
    for(j=0; j < PMAX ; j++){
        (*level).plateau[i][j].etat = 0;
        (*level).plateau[i][j].tire.direction = 0;
    }
    }

    /*le joueur en place */
    (*level).plateau[0][20].etat = 1;

}

void theGameRunner(theGame *level){

    int quit = 0;
    struct timespec last , neww;
    double accum ;


    MLV_Keyboard_button sym = MLV_KEYBOARD_NONE;
    MLV_Button_state state;
    MLV_Event event;


    while(quit == 0){

        /* get the time in nanosecond at the frame beginning */
        clock_gettime(CLOCK_REALTIME,&last);
        /* display if the current frame */

        event = MLV_get_event(&sym,NULL, NULL, NULL, NULL, NULL , NULL , NULL, &state);

        /* on regle tout ici */
        if( event == MLV_KEY){

            if( state == MLV_PRESSED){

                switch( sym){
                    /* 6 cas , haut , bas , gauche
                    droite , espace et echap */
                case MLV_KEYBOARD_UP:

                    if( DEBUG){
                        fprintf(stdout,"UP\n");
                    }
                    /* y -1 */
                    if( ((*level).heros.posy - 1)  > 0){
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 0;
                        (*level).heros.posy--;
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 1;
                    }



                    break;
                case MLV_KEYBOARD_DOWN:
                    if( DEBUG){
                        fprintf(stdout,"DOWN\n");
                    }
                    /* y  +1 */

                    if( ((*level).heros.posy + 1)  < PMAX){
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 0;
                        (*level).heros.posy++;
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 1;
                    }



                    break;
                case MLV_KEYBOARD_LEFT:
                    if( DEBUG){
                        fprintf(stdout,"LEFT\n");
                    }
                    /* x   - 1 */
                    if( ((*level).heros.posx - 1)  > 0){
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 0;
                        (*level).heros.posx--;
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 1;
                    }


                    break;
                case MLV_KEYBOARD_RIGHT:
                    if( DEBUG){
                        fprintf(stdout,"RIGHT\n");
                    }
                    /* y   - 1 */
                    if( ((*level).heros.posx + 1)  < PMAX){
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 0;
                        (*level).heros.posx++;
                        (*level).plateau[(*level).heros.posx][(*level).heros.posy].etat = 1;
                    }

                    fflush(stdin);
                    break;
                case MLV_KEYBOARD_SPACE:
                    if( DEBUG){
                        fprintf(stdout,"SPACE\n");
                    }

                    if( ((*level).heros.posx + 1) < PMAX){
                        (*level).plateau[(*level).heros.posx+1][(*level).heros.posy].tire.direction = 1;
                    }
                    break;
                case MLV_KEYBOARD_ESCAPE:
                        quit = 1;
                    break;
                default:
                    /* do nothing */
                    break;
                }

            }

        }



        /* get the time */
        clock_gettime(CLOCK_REALTIME, &neww);
        /* we compute here the time spent for the current frame */
        accum = (neww.tv_sec - last.tv_sec) + ((neww.tv_nsec - last.tv_nsec)/BILLION);

        /* we force here to wait if the frame was too short */
        if( accum < (1.0/FPS)){
            MLV_wait_milliseconds((int)(((1.0/FPS)- accum)+1000));
        }

        cleanFire(&*level);
        cleanCollision(&*level);
        showWorld(&*level);
        ennemiShow(&*level);
        quit = win(&*level);
    }


}

int win(theGame *level){

    if( (*level).nbEnnemi > 0 && (*level).heros.vie > 0 && (*level).heros.population > 0)
        return 0;
    if( (*level).heros.population <=  0  || (*level).heros.vie <=0 || (*level).nbEnnemi <= 0)
        return 1;

    return 1;

}


/* sous fonction ennemi */

void ennemiS(theGame *level){

    if( (rand()%DIFFICULTE) == 0){

        int i;

        for(i=0; i < (*level).nbEnnemiInit ; i++){

            if( (*level).tas[i].posx == -1 && (*level).tas[i].vie > 0){

                (*level).tas[i].posx = PMAX - 1;
                (*level).tas[i].posy = PMAX / 2;

                (*level).plateau[PMAX-1][PMAX/2].etat = 2;

                i = (*level).nbEnnemiInit;

            }
        }

    }

}

void ennemiShow(theGame *level){


    /* fait apparaitre les ennemis et les bouger */
    ennemiS(&*level);

    int i, move, x , y;

    for(i=0; i < (*level).nbEnnemiInit ; i++){

        if( (*level).tas[i].posx > -1){
            x = (*level).tas[i].posx;
            y = (*level).tas[i].posy;
            move = ennemiMove( y-1 < 0 ? 1:(*level).plateau[x][y-1].etat, y+1 > PMAX ? 1: (*level).plateau[x][y+1].etat, x-1 < 0 ? 1: (*level).plateau[x-1][y].etat, y);


            switch(move){
            /* 0 tir, 1 haut 2 bas 3 gauche */
            case 0:

                (*level).plateau[x-1][y].tire.direction = 2;

                break;
            case 1:

                (*level).plateau[x][y-1].etat = 2;
                (*level).plateau[x][y].etat = 0;
                (*level).tas[i].posy = y-1;

                break;
            case 2:

                (*level).plateau[x][y+1].etat = 2;
                (*level).plateau[x][y].etat = 0;
                (*level).tas[i].posy = y+1;

                break;
            case 3:

                (*level).plateau[x-1][y].etat = 2;
                (*level).plateau[x][y].etat = 0;
                (*level).tas[i].posx = x-1;

                break;





            }
        }

    }
    /* un ennemi apparait sil y en a de vivant et quil est à -1 sur ses coords */


    /* on les fait bouger */






}

void showWorld(theGame *level){


    MLV_Image *fond , *heros , *ennemi , *missile;

    fond = MLV_load_image("rsc/menuCiel.jpg");
    heros = MLV_load_image("rsc/heros.png");
    ennemi = MLV_load_image("rsc/ennemi.png");
    missile = MLV_load_image("rsc/missile.png");


    /* 80 % de l'ecran , 20 % du bas est reservée */

    if( fond == NULL){
        fprintf(stderr,"erreur chargement fond \n");
        exit(EXIT_FAILURE);
    }

    if( heros == NULL){
        fprintf(stderr,"erreur chargement heros \n");
        exit(EXIT_FAILURE);
    }


    if( ennemi == NULL){
        fprintf(stderr,"erreur chargement ennemi \n");
        exit(EXIT_FAILURE);
    }


    if( missile == NULL){
        fprintf(stderr,"erreur chargement missile \n");
        exit(EXIT_FAILURE);
    }


    int i,j;
    int sizex = RESX/PMAX;
    int sizey = (RESY*0.80)/PMAX;

    MLV_resize_image(fond,RESX, RESY*0.80);
    MLV_resize_image(heros,RESX/PMAX,(RESY*0.80)/PMAX);
    MLV_resize_image(ennemi,RESX/PMAX,(RESY*0.80)/PMAX);
    MLV_resize_image(missile,RESX/PMAX,(RESY*0.80)/PMAX);


    MLV_draw_image(fond,0,0);

    for(i=0; i < PMAX ; i++){
        for(j=0; j < PMAX ; j++){

            if( (*level).plateau[i][j].etat == 1 ){
                /* joueur */
                MLV_draw_image(heros,i*sizex,j*sizey);
            }
            else if((*level).plateau[i][j].etat == 2){
                /* ennemi */
                MLV_draw_image(ennemi,i*sizex,j*sizey);
            }
            else if((*level).plateau[i][j].tire.direction == 1 || (*level).plateau[i][j].tire.direction == 2){
                MLV_draw_image(missile,i*sizex,j*sizey);
            }
        }
    }

    MLV_free_image(fond);
    MLV_free_image(heros);
    MLV_free_image(ennemi);
    MLV_free_image(missile);

    MLV_draw_filled_rectangle(0,RESY*0.80,RESX,RESY*0.20,MLV_COLOR_BLACK);

    showStat(&*level);

    MLV_actualise_window();

}

void showStat(theGame *level){

    /* on affiche sommairement les infos du jeu */

    char tmp[64]={'\0',};






    MLV_draw_text(RESX*0.10, RESY*0.80, " Niveau ", MLV_COLOR_RED);
    sprintf(tmp,"%d",(*level).level);
    MLV_draw_text(RESX*0.10, RESY*0.85,tmp, MLV_COLOR_RED );


    MLV_draw_text(RESX*0.20, RESY*0.80, " Ennemi restant ", MLV_COLOR_RED);
    sprintf(tmp,"%d",(*level).nbEnnemi);
    MLV_draw_text(RESX*0.20 ,RESY*0.85,tmp, MLV_COLOR_RED );



    MLV_draw_text(RESX*0.30, RESY*0.80, " Vie restante ", MLV_COLOR_RED);
    sprintf(tmp,"%d",(*level).heros.vie);
    MLV_draw_text(RESX*0.30, RESY*0.85,tmp, MLV_COLOR_RED );



    MLV_draw_text(RESX*0.40, RESY*0.80, " Bouclier restant ", MLV_COLOR_RED);
    sprintf(tmp,"%d",(*level).heros.bouclier);
    MLV_draw_text(RESX*0.40, RESY*0.85,tmp, MLV_COLOR_RED );


    MLV_draw_text(RESX*0.50, RESY*0.80, " Population restante ", MLV_COLOR_RED);
    sprintf(tmp,"%d",(*level).heros.population);
    MLV_draw_text(RESX*0.50, RESY*0.85,tmp, MLV_COLOR_RED );

    MLV_draw_text(RESX*0.70 , RESY*0.80, " Echap pour quitter ", MLV_COLOR_RED);

}

void cleanFire(theGame *level){

    int i,j, direction;

    /* on fait avancer les tirs */
    static int tmp[PMAX][PMAX];

    for(i=0; i < PMAX; i++)
        for(j=0; j < PMAX ; j++)
            tmp[i][j]=0;

    for(i=0; i < PMAX ; i++){
        for(j=0; j < PMAX ; j++){

                direction = (*level).plateau[i][j].tire.direction;

                if( DEBUG && direction > 0){
                    fprintf(stdout,"%d %d a %d direction \n",direction,i,j);
                }

                switch(direction){

                case 1:
                    /* on va a droite */
                    /* on verifie s'il n'y a pas dire à cote */
                    if( i+1 > PMAX){
                            /* hors cadre on le supprime ... */
                            tmp[i][j] = 0;
                        /*(*level).plateau[i][j].tire.direction = 0;*/
                    }
                    else{
                    if((*level).plateau[i+1][j].tire.direction == 2){
                        /* on annule les deux */
                        tmp[i][j] = 0;
                        tmp[i+1][j]  = 0;
                        /*(*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i+1][j].tire.direction = 0;*/
                    }
                    else{

                        tmp[i][j] = 0;
                        tmp[i+1][j] = 1;
                        /*(*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i+1][j].tire.direction = 1;*/
                    }
                    }
                    break;
                case 2:
                    /* on va a gauche */
                    /* on verifie s'il n'y a pas dire à cote */

                    if( i-1 < 0){

                        tmp[i][j] = 0;
                        /*(*level).plateau[i][j].tire.direction = 0;*/
                    }
                    else{
                    if((*level).plateau[i-1][j].tire.direction == 1){
                        /* on annule les deux */

                        tmp[i][j] = 0;
                        tmp[i-1][j] = 0;
                        /*(*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i-1][j].tire.direction = 0;*/
                    }
                    else{
                         tmp[i][j] = 0;
                         tmp[i-1][j] = 2;
                        /*(*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i-1][j].tire.direction = 2;*/
                    }
                    }
                    break;
                case 3:
                    /* on va en haut */
                    /* on verifie s'il n'y a pas dire à cote */
                    if( j-1 < 0){
                        (*level).plateau[i][j].tire.direction  = 0;
                    }
                    else{
                    if((*level).plateau[i][j-1].tire.direction > 0){
                        /* on annule les deux */
                        (*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i][j-1].tire.direction = 0;
                    }
                    else{
                        (*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i][j-1].tire.direction = 3;
                    }
                    }
                    break;
                case 4:
                    /* on va en bas */
                    /* on verifie s'il n'y a pas dire à cote */
                    if( j+1 > PMAX){
                        (*level).plateau[i][j].tire.direction = 0;
                    }
                    else{
                    if((*level).plateau[i][j+1].tire.direction > 0){
                        /* on annule les deux */
                        (*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i][j+1].tire.direction = 0;
                    }
                    else{
                        (*level).plateau[i][j].tire.direction = 0;
                        (*level).plateau[i][j+1].tire.direction = 4;
                    }
                    }
                    break;
                default:
                        /* on ne fait rien ici */
                    break;
                }

        }
    }

    for(i=0; i < PMAX ; i++)
        for(j=0; j < PMAX ; j++)
            (*level).plateau[i][j].tire.direction = tmp[i][j];

}

void cleanCollision(theGame *level){

    int i,j, tmp;

    for(i=0; i < PMAX ; i++){
        for(j=0; j < PMAX ; j++){

            /* si un ennemi a reussi a venir tout a gauche */
            if( (*level).plateau[i][j].etat == 2 && i == 0 ){

                (*level).heros.population--;
                (*level).nbEnnemi--;
                (*level).plateau[i][j].etat = 0;
                (*level).plateau[i][j].tire.direction = 0;
                /* on elemine l'ennemi */

                tmp = ennemi(((*level).tas),(*level).nbEnnemiInit,i,j);

                if(DEBUG){
                    fprintf(stdout,"%d tmp cleanCollision ennemiEnd",tmp);
                }

                (*level).tas[tmp].vie = 0;
                (*level).tas[tmp].posx = -1;
                (*level).tas[tmp].posy = -1;
                (*level).nbEnnemi--;


            }

            if( (*level).plateau[i][j].tire.direction > 0 && (*level).plateau[i][j].etat > 0){
                /* collision ... */
                (*level).plateau[i][j].tire.direction = 0;

                if((*level).plateau[i][j].etat == 1){
                    /* joueur */

                    if((*level).heros.bouclier > 0){
                        (*level).heros.bouclier--;
                    }
                    else{
                        (*level).heros.vie--;

                    }


                }
                else if((*level).plateau[i][j].etat == 2){
                    /* ennemi */

                    /* on elemine l'ennemi */
                tmp = ennemi(((*level).tas),(*level).nbEnnemiInit,i,j);

                if(DEBUG){
                    fprintf(stdout,"%d tmp cleanCollision ennemimissile",tmp);
                }
                (*level).plateau[i][j].tire.direction = 0;
                (*level).plateau[i][j].etat = 0;
                (*level).tas[tmp].vie = 0;
                (*level).tas[tmp].posx = -1;
                (*level).tas[tmp].posy = -1;
                (*level).nbEnnemi--;



                }

            }
        }
    }

}

void levelC(theGame *level){

    if( DEBUG){
        fprintf(stderr,"Entre dans levelC \n");
    }


    FILE *file = NULL;
    int i;
    char option[MAXCHAR];

    file = fopen("level.info","r");

    if(file == NULL){
        fprintf(stderr,"Erreur ouverture level.info...\n");
        file = fopen("level.info","w+");

        if( file == NULL){
            fprintf(stderr,"Erreur creation level.info , on arrete ... \n");
            exit(EXIT_FAILURE);
        }
    }


    for(i=0; i < MAXLEVEL ; i++){

        if(fgets(option,9,file) != NULL){
            if(strstr(option,"on") != NULL){
                (*level).lvlInfo[i] = 1;
            }
            else{
                (*level).lvlInfo[i] = 0;
            }
        }
        else{
            (*level).lvlInfo[i] = 0;
        }


    }

    drawLevel(&*level);

    MLV_actualise_window();
    free(file);
}

void drawLevel(theGame *level){

    int i = 0;
    int sizex = RESX/(MAXLEVEL/5);
    int sizey = RESY/(MAXLEVEL/5);
    char str[MAXCHAR];

    printf("%d \n",i);

    for(i = 0 ; i < MAXLEVEL ; i++){
    if( (*level).lvlInfo[i] == 1){
        sprintf(str,"%d on",i+1);
    }
    else{
        sprintf(str,"%d off",i+1);
    }

    if( (*level).lvlInfo[i] == 1){
        MLV_draw_filled_rectangle(((i)%5)*sizex,((i)/5)*sizey,sizex,sizey,MLV_COLOR_BLUE);
        MLV_draw_adapted_text_box(((i)%5)*sizex,((i)/5)*sizey,str,9, MLV_COLOR_BLUE, MLV_COLOR_GREEN, MLV_COLOR_BLUE,MLV_TEXT_CENTER);
    }
    else{
        MLV_draw_filled_rectangle(((i)%5)*sizex,((i)/5)*sizey,sizex,sizey,MLV_COLOR_RED);
        MLV_draw_adapted_text_box(((i)%5)*sizex,((i)/5)*sizey,str,9, MLV_COLOR_RED, MLV_COLOR_GREEN, MLV_COLOR_RED,MLV_TEXT_CENTER);
    }

    }

    MLV_actualise_window();
    chooseMe(&*level);

}

void chooseMe(theGame *level){


    int a , b;
    int sizex = RESX/(MAXLEVEL/5);
    int sizey = RESY/(MAXLEVEL/5);
    int pos = 0;
    int i, allow = 1;

    MLV_wait_mouse(&a,&b);


    while( 1 == 1){

        if( a < (sizex*5) && b < (sizey*6)){
        pos = a / (sizex) + ((b/sizey)*5) + 1;

        if( DEBUG){
        printf("pos  %d \n",pos);
        }

        for(i=0; i < (pos-1) ; i++){

            if( (*level).lvlInfo[i] == 0){
                allow = 0;
            }
        }

        if( allow == 0 ){
            allow = 1;
        }
        else{

            if(DEBUG){
                printf("allow %d ,, pos %d \n",allow,pos);
            }


            (*level).level = pos;
            return ;
        }


        }



        MLV_wait_mouse(&a,&b);



    }

}
