#include "menu.h"



/* tout ce passe la dedans */
void menu(){

    MLV_create_window(nomFenetre,nomIcone,RESX,RESY);


    int menu = afficheMenu();



    while(menu != -1){

        if( menu == 0){
            /* lancer le jeu */
            chooseLevel();

        }
        else if( menu == 1){
            /* montrer les scores */
            afficheScore();
        }
        else if(menu == 2){
            afficheCredit();
            /*montrer les credits */
        }



        menu = afficheMenu();
    }


    MLV_free_window();
}


int afficheMenu(){

    afficheFondMenu();
    afficheOption();

    return choixOption();

}


void afficheOption(){

    MLV_Image *start , *score , *quitter , *credit;


    start = MLV_load_image("rsc/start.png");
    score = MLV_load_image("rsc/score.png");
    quitter = MLV_load_image("rsc/quitter.png");
    credit = MLV_load_image("rsc/credit.png");

    if( start == NULL ){
        fprintf(stderr,"start introuvable \n");
        exit(EXIT_FAILURE);
    }
    if( score == NULL ){
        fprintf(stderr,"score introuvable \n");
        exit(EXIT_FAILURE);
    }
    if( quitter == NULL ){
        fprintf(stderr,"quitter introuvable \n");
        exit(EXIT_FAILURE);
    }
    if( credit == NULL ){
        fprintf(stderr,"credit introuvable \n");
        exit(EXIT_FAILURE);
    }

    MLV_resize_image(start,RESX*0.20, RESY*0.10);
    MLV_resize_image(quitter,RESX*0.20, RESY*0.10);
    MLV_resize_image(score,RESX*0.20, RESY*0.10);
    MLV_resize_image(credit,RESX*0.20, RESY*0.10);

    MLV_draw_image(start, RESX*0.40 , RESY*0.40);
    MLV_draw_image(quitter, 0, RESY*0.90);
    MLV_draw_image(score, RESX*0.40, RESY*0.50);
    MLV_draw_image(credit,RESX*0.40, RESY*0.60);

    MLV_actualise_window();

    MLV_free_image(score);
    MLV_free_image(quitter);
    MLV_free_image(start);
    MLV_free_image(credit);



}

void afficheFondMenu(){

    MLV_Image *tmp = NULL;

    tmp = MLV_load_image("rsc/menuCiel.jpg");

    if( tmp == NULL){
        fprintf(stderr,"cielEtoile introuvable \n");
        exit(EXIT_FAILURE);
    }

    MLV_resize_image(tmp,RESX,RESY);

    MLV_draw_image(tmp,0,0);

    MLV_actualise_window();

    MLV_free_image(tmp);
}


int choixOption(){

    int a,b;

    while( 1 == 1){

        MLV_wait_mouse(&a,&b);

        /* start */

        if( a > RESX*0.40 && a < RESX*0.60 && b > RESY*0.40 && b < RESY*0.50)
            return 0;

        /*scores */
        if( a > RESX*0.40 && a < RESX*0.60 && b > RESY*0.50 && b < RESY*0.60)
            return 1;
        /*credit */
        if( a > RESX*0.40 && a < RESX*0.60 && b > RESY*0.60 && b < RESY*0.70)
            return 2;
        /*quitter */
        if( a > 0 && a < RESX*0.20 && b > RESY*0.90 && b < RESY)
            return -1;


    }


}
