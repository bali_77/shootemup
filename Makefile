CC = gcc
CFLAGS =  -Wall -lMLV
EXEC = ShootEmUp 
OBJETS = credit.o ennemi.o game.o main.o menu.o score.o 

ShootEmUp: $(OBJETS)
	$(CC) -o $(EXEC) $(OBJETS) $(CFLAGS) 

main.o: main.c menu.h
	$(CC) -c $< $(CFLAGS) -o main.o
menu.o: menu.c menu.h config.h game.h credit.h score.h
	$(CC) -c $< $(CFLAGS) -o menu.o
game.o: game.c game.h joueur.h ennemi.h case.h config.h
	$(CC) -c $< $(CFLAGS) -o game.o
credit.o: credit.c credit.h config.h
	$(CC) -c $< $(CFLAGS) -o credit.o
score.o: score.c score.h config.h
	$(CC) -c $< $(CFLAGS) -o score.o
ennemi.o: ennemi.c ennemi.h
	$(CC) -c $< $(CFLAGS) -o ennemi.o

clean:
	rm -f *.o
